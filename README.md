## This add-on is no longer maintained, please check GP dopesheet add-on repository to work with dopesheets

# GP Move column

Blender add-on, allows to move a keyframe column through regular and grease pencil dopesheets

# [Download latest as zip](https://gitlab.com/andarta-pictures/gp-move-column/-/archive/master/gp-move-column-master.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list

# User guide

Select at least one keyframe in the dopesheet, then right-click to open the dopesheet context menu and select Move column.

![guide](docs/move_column.png "guide")  

# Warning

This tool is in early development stage, thank you for giving it a try ! please report any bug to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)
