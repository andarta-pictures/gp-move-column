# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "GP move column",
    "author" : "Tom VIGUIER",
    "description" : "move keyframes in both grease pencil an regular Dopesheets",
    "blender" : (2, 91, 0),
    "version" : (0, 1, 0),
    "location" : "Dopesheet context menu",
    "warning" : "This tool is in early development stage, thank you for giving it a try ! please report any bug to t.viguier@andarta-pictures.com",
    "category" : "Andarta",
    "tracker_url": "https://gitlab.com/andarta-pictures/gp-move-column/-/issues",
}

import bpy
from bpy.props import IntProperty, FloatProperty
from bpy.types import Operator, Menu

class DOPESHEET_OT_movecolumn(bpy.types.Operator):
    bl_idname = "dopesheet.movecolumn"
    bl_label = "Move keyframes column in both GP and regular Dope Sheets"
    prev_dif_pos_x : bpy.props.FloatProperty()
    dif_pos_x : bpy.props.FloatProperty()

    def list_gp_selected_kf(self) :
        for layer in self.gp_ob.layers :
            for frame in layer.frames :
                if frame.select :
                    if frame.frame_number not in self.frame_numbers :
                        self.frame_numbers.append(frame.frame_number)
                          
    def list_gp_kf(self, frame_number):
        for layer in self.gp_ob.layers:
            for frame in layer.frames :
                if frame.frame_number == frame_number :
                    if frame not in self.gp_frames :
                        self.gp_frames.append(frame)
                                               
    def list_ob_selected_kf(self):
        for fcurve in self.ob.animation_data.action.fcurves :
            for kf in fcurve.keyframe_points :
                if kf.select_control_point :
                    if int(kf.co.x) not in self.frame_numbers :
                        self.frame_numbers.append(int(kf.co.x))
                        
    def list_ob_kf(self, frame_number) :
        for fcurve in self.ob.animation_data.action.fcurves :
            for kf in fcurve.keyframe_points :
                if kf.co.x == frame_number :
                    self.ob_frames.append(kf)
        
    def modal(self, context, event):
        
        if event.type == 'MOUSEMOVE':
            #cancel previous movement
            for frame in self.gp_frames :
                frame.frame_number += self.prev_dif_pos_x
            for frame in self.ob_frames :
                frame.co.x += self.prev_dif_pos_x
            
            #calculate new movement
            self.dif_pos_x = int((self.initial_mouse - event.mouse_x)/4)
            self.prev_dif_pos_x = self.dif_pos_x
            
            #apply new movement  
            for frame in self.gp_frames :
                frame.frame_number -= self.dif_pos_x
            for frame in self.ob_frames :
                frame.co.x -= self.dif_pos_x

        elif event.type == 'LEFTMOUSE':
            refresh_dopesheets()
            return {'FINISHED'}

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            #cancel previous movement
            for frame in self.gp_frames :
                frame.frame_number += self.prev_dif_pos_x
            for frame in self.ob_frames :
                frame.co.x += self.prev_dif_pos_x
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}
    
    def invoke(self, context, event):
        
        self.scene = bpy.context.scene
        self.time = self.scene.frame_current
        
        self.initial_mouse = event.mouse_x
        self.prev_dif_pos_x = 0.0
        self.dif_pos_x = 0.0

        if context.space_data.type == 'DOPESHEET_EDITOR':
            self.frame_numbers = list()
            self.gp_frames = list()
            self.ob_frames = list()
            for ob in bpy.context.selected_objects :
                self.ob = ob
                if self.ob.type == 'GPENCIL':
                    self.gp_ob = self.ob.data.id_data
                    self.dopesheet = context.area.spaces[0]

                    #get the frame_number of selected frames
                    if self.dopesheet.mode == 'GPENCIL' :
                        self.list_gp_selected_kf()
                    if self.dopesheet.mode == 'DOPESHEET' :
                        self.list_ob_selected_kf()
                        
                    #append corresponding keyframes to lists
                    for frame_number in self.frame_numbers :                        
                        self.list_gp_kf(frame_number)
                        self.list_ob_kf(frame_number)

            context.window_manager.modal_handler_add(self)      
            return {'RUNNING_MODAL'}
        
        else:
            print('error - wrong context (dopesheet needed)')
            return {'CANCELLED'}

class DOPESHEET_OT_delete_column(bpy.types.Operator):
    bl_idname = "dopesheet.delete_column"
    bl_label = "delete keyframes column"
    prev_dif_pos_x : bpy.props.FloatProperty()
    dif_pos_x : bpy.props.FloatProperty()

    def list_gp_selected_kf(self) :
        for layer in self.gp_ob.layers :
            for frame in layer.frames :
                if frame.select :
                    if frame.frame_number not in self.frame_numbers :
                        self.frame_numbers.append(frame.frame_number)
                          
    def list_gp_kf(self, frame_number):
        for layer in self.gp_ob.layers:
            for frame in layer.frames :
                if frame.frame_number == frame_number :
                    if frame not in self.gp_frames :
                        self.gp_frames.append(frame)
                                               
    def list_ob_selected_kf(self):
        for fcurve in self.ob.animation_data.action.fcurves :
            for kf in fcurve.keyframe_points :
                if kf.select_control_point :
                    if int(kf.co.x) not in self.frame_numbers :
                        self.frame_numbers.append(int(kf.co.x))
                        
    def list_ob_kf(self, frame_number) :
        for fcurve in self.ob.animation_data.action.fcurves :
            for kf in fcurve.keyframe_points :
                if kf.co.x == frame_number :
                    self.ob_frames.append(kf)
        
    def invoke(self, context, event):
        print('invoke=================')
        wm = context.window_manager
        self.scene = bpy.context.scene
        self.time = self.scene.frame_current
        
        self.initial_mouse = event.mouse_x
        self.prev_dif_pos_x = 0.0
        self.dif_pos_x = 0.0

        if context.space_data.type == 'DOPESHEET_EDITOR':
            self.frame_numbers = list()
            self.gp_frames = list()
            self.ob_frames = list()
            for ob in bpy.context.selected_objects :
                self.ob = ob
                if self.ob.type == 'GPENCIL':
                    self.gp_ob = self.ob.data.id_data
                    self.dopesheet = context.area.spaces[0]

                    #get the frame_number of selected frames
                    if self.dopesheet.mode == 'GPENCIL' :
                        self.list_gp_selected_kf()
                    if self.dopesheet.mode == 'DOPESHEET' :
                        self.list_ob_selected_kf()
                        
                    #append corresponding keyframes to lists
                    for frame_number in self.frame_numbers :                        
                        self.list_gp_kf(frame_number)
                        self.list_ob_kf(frame_number)
            return wm.invoke_props_dialog(self)
        
        else:
            print('error - wrong context (dopesheet needed)')
            return {'CANCELLED'}

    def draw(self,context):
        layout = self.layout
        layout.label(text = 'Confirm')

    def execute(self, context) :
        print('execute================')
        for ob in bpy.context.selected_objects :
            if ob.animation_data :
                for fcurve in ob.animation_data.action.fcurves :
                    for frame in fcurve.keyframe_points :
                        if frame in self.ob_frames :
                            fcurve.keyframe_points.remove(frame)
            if ob.type == 'GPENCIL' :
                gp_ob = ob.data  
                for layer in gp_ob.layers :
                    for frame in layer.frames : 
                        if frame in self.gp_frames :
                            layer.frames.remove(frame)
            

        refresh_dopesheets()
        return {'FINISHED'}


def refresh_dopesheets():
    refresh_GP_dopesheet()
    refresh_DS_dopesheet()

def refresh_GP_dopesheet() :  
    #dirty way to force blender to refresh frames indices in grease pencil dopesheet
    if bpy.context.object.type == 'GPENCIL' :
        cur_areatype = str(bpy.context.area.type)
        bpy.context.area.type = 'DOPESHEET_EDITOR'
        cur_space_mode = str(bpy.context.area.spaces[0].mode)
        bpy.context.area.spaces[0].mode = 'GPENCIL'
        bpy.ops.action.mirror(type = 'XAXIS')
        bpy.ops.action.mirror(type = 'XAXIS')
        bpy.context.area.spaces[0].mode = cur_space_mode
        bpy.context.area.type = cur_areatype

def refresh_DS_dopesheet() :  
    #dirty way to force blender to refresh frames indices in regular dopesheet
    if bpy.context.object.type == 'GPENCIL' :
        cur_areatype = str(bpy.context.area.type)
        bpy.context.area.type = 'DOPESHEET_EDITOR'
        cur_space_mode = str(bpy.context.area.spaces[0].mode)
        bpy.context.area.spaces[0].mode = 'DOPESHEET'
        bpy.ops.action.mirror(type = 'XAXIS')
        bpy.ops.action.mirror(type = 'XAXIS')
        bpy.context.area.spaces[0].mode = cur_space_mode
        bpy.context.area.type = cur_areatype


def draw_menu(self, context):
    layout = self.layout
    layout.separator()
    layout.operator_context = 'INVOKE_DEFAULT'
    layout.operator(DOPESHEET_OT_movecolumn.bl_idname, text="Move column")
    layout.operator(DOPESHEET_OT_delete_column.bl_idname, text="Delete column")

def register():
    bpy.types.DOPESHEET_MT_context_menu.append(draw_menu)
    bpy.utils.register_class(DOPESHEET_OT_movecolumn)
    bpy.utils.register_class(DOPESHEET_OT_delete_column)

    
    
def unregister():
    bpy.types.DOPESHEET_MT_context_menu.remove(draw_menu)
    bpy.utils.unregister_class(DOPESHEET_OT_movecolumn)
    bpy.utils.unregister_class(DOPESHEET_OT_delete_column)
    
if __name__ == "__main__":
    register()
    
